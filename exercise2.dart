void main(List<String> args) {
  List winningApps = [
    "FNB Banking",
    "SnapScan",
    "Live-inspect",
    "Wumdrop",
    "Domestly",
    "Shyft",
    "Khula ecosystem",
    "Naked Insurance",
    "EasyEquities",
    "Ambani Africa"
  ];

  List winningAppsAndYears = [
    "FNB Banking : 2012",
    "Snapscan : 2013",
    "Live-inspect : 2014",
    "Wumdrop : 2015",
    "Domestly : 2016",
    "Shyft : 2017",
    "Khula ecosystem: 2018",
    "Naked Insurance : 2019",
    "Easy Equities : 2020",
    "Ambani Africa : 2021"
  ];

  winningAppsAndYears.sort();

  print(
      "MTN Business App of the Year Award winners since 2012 sorted by name are: $winningAppsAndYears");
  print("The winning app in 2017 is " + winningApps[5]);
  print("The winning app in 2018 is " + winningApps[6]);

  int counter = winningApps.length;
  print("The total number of apps is $counter");
}
