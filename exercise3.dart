class Apps {
  List names = [
    "FNB Banking",
    "SnapScan",
    "Live-inspect",
    "Wumdrop",
    "Domestly",
    "Shyft",
    "Khula ecosystem",
    "Naked Insurance",
    "EasyEquities",
    "Ambani Africa"
  ];
  List category = [
    "Financinal/Blackberry App",
    "Financial App",
    "Android Enterprise",
    "Enterprise App",
    "Consumer App",
    "Financial solution",
    "Agriculture Solution",
    "Financial Solution",
    "Consumer Solution",
    "South African Solution"
  ];
  List developers = [
    "FNB",
    "Lobus Ehlers and Gerrit Greeff",
    "Lightstone Auto",
    "Benjamin Claassen and Muneeb Samuels",
    "Berno Pitgieter and Thatoyaona Marumo",
    "Standard Bank",
    "Karidas Tshintsholo, Mathew Piper and Jackson Dyora",
    "Sumarie Greybe, Ernest North and Alex Thomson",
    "Purple Group",
    "Mukundi Lambani",
  ];
  List? year = [
    "2012",
    "2013",
    "2014",
    "2015",
    "2016",
    "2017",
    "2018",
    "2019",
    "2020",
    "2021"
  ];

  void allCaps() {
    names = names
        .map((ns) => ns.toUpperCase())
        .toList(); //goes through all the names and capitalizes the letters
    print("This function prints all the app names in caps: $names");
  }
}

void main() {
  Apps obj = new Apps();

  for (int e = 0; e < 10; e++) {
    print("The year the award was won: " + obj.year![e]);
    print("Name of the winning App: " + obj.names[e]);
    print("Category/Sector: " + obj.category[e]);
    print("Developers: " + obj.developers[e]);
    print("\n");
  }

  obj.allCaps();
}
